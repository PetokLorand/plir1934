package note.utils;

public class ClasaException extends Exception {

    public  ClasaException(String message) {
        super(new String(message));
    }
}
